package mx.sidlors.sidlors.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SidlorsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SidlorsDemoApplication.class, args);
	}

}
